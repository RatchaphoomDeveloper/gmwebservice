﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Management;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model;
using Model.AppConfig;

namespace gmprojectbackend.Controllers
{
    [Authorize]
    public class GmController : ControllerBase
    {

        [AllowAnonymous]
        [HttpPost]
        [Route("Gm/Login")]
        public IActionResult Login([FromBody] LoginModel loginData)
        {
            var output = new OutputModel();
            try
            {
                output = LoginManagement.Ins.LoginMng(loginData);
                if (output.Status == SystemStatus.SUCCESS)
                {
                    return Ok(output.Data);
                }
                else
                {
                    return BadRequest(new { Message = output.Message + "" });
                }
            }
            catch (Exception ex)
            {
                output.Status = SystemStatus.ERROR;
                output.Message = "GM Service [Gm/Login] : " + ex.Message;
                return BadRequest(new { Message = output.Message + "" });
            }
            finally
            {
                Utility.Ins.WriteToLog("GM", "Login", loginData, output);
            }

        }

       

        [AllowAnonymous]
        [HttpPost]
        [Route("Gm/Register")]
        public IActionResult Register([FromBody] UserModel userData)
        {
            var output = new OutputModel();
            try
            {
                output = RegisterManagement.Ins.RegisterMng(userData);
                if (output.Status == SystemStatus.SUCCESS)
                {
                    return Ok(new { Message = output.Message + "" });
                }
                else
                {
                    return BadRequest(new { Message = output.Message + "" });
                }
            }
            catch (Exception ex)
            {
                output.Status = SystemStatus.ERROR;
                output.Message = "GM Service [GM/Register] : " + ex.Message;
                return BadRequest(new { Message = output.Message + "" });
            }
            finally
            {
                Utility.Ins.WriteToLog("GM", "Register", userData, output);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Gm/ChangePassword")]
        public IActionResult ChangePassword([FromBody] LoginModel loginData)
        {
            var output = new OutputModel();
            try
            {
                output = LoginManagement.Ins.ChangePasswordMng(loginData);
                if (output.Status == SystemStatus.SUCCESS)
                {
                    return Ok(new { Message = output.Message + "" });
                }
                else
                {
                    return BadRequest(new { Message = output.Message + "" });
                }
            }
            catch (Exception ex)
            {
                output.Status = SystemStatus.ERROR;
                output.Message = "GM Service [GM/ChangePassword] : " + ex.Message;
                return BadRequest(new { Message = output.Message + "" });
            }
            finally
            {
                Utility.Ins.WriteToLog("GM", "ChangePassword", loginData, output);
            }

        }


        [AllowAnonymous]
        [HttpPost]
        [Route("Gm/CheckUser")]
        public IActionResult CheckUser([FromBody] LoginModel loginData)
        {
            var output = new OutputModel();
            try
            {
                output = LoginManagement.Ins.CheckUserMng(loginData);
                if (output.Status == SystemStatus.SUCCESS)
                {
                    return Ok(output.Data);
                }
                else
                {
                    return BadRequest(new { Message = output.Message + "" });
                }
            }
            catch (Exception ex)
            {
                output.Status = SystemStatus.ERROR;
                output.Message = "GM Service [GM/CheckUser] : " + ex.Message;
                return BadRequest(new { Message = output.Message + "" });
            }
            finally
            {
                Utility.Ins.WriteToLog("GM", "CheckUser", loginData, output);
            }

        }
  
    [HttpPost]
    [Route("Gm/CheckUserStatus")]
    public IActionResult CheckUserStatus([FromBody] UserStatusModel userStatusData)
    {
      var output = new OutputModel();
      try
      {
        output = LoginManagement.Ins.CheckUserStatusMng(userStatusData);
        if (output.Status == SystemStatus.SUCCESS)
        {
          return Ok(output.Data);
        }
        else
        {
          return BadRequest(new { Message = output.Message + "" });
        }
      }
      catch (Exception ex)
      {
        output.Status = SystemStatus.ERROR;
        output.Message = "GM Service [GM/CheckUserStatus] : " + ex.Message;
        return BadRequest(new { Message = output.Message + "" });
      }
      finally
      {
        Utility.Ins.WriteToLog("GM", "CheckUserStatus", userStatusData, output);
      }

    }

    [HttpPost]
    [Route("Gm/InsUserFeedBack")]
    public IActionResult InsUserFeedBack([FromBody] FeedBackModel feedBackData)
    {
      var output = new OutputModel();
      try
      {
        output = LoginManagement.Ins.InsUserFeedBack(feedBackData);
        if (output.Status == SystemStatus.SUCCESS)
        {
          return Ok(new { Message = output.Message + "" });
        }
        else
        {
          return BadRequest(new { Message = output.Message + "" });
        }
      }
      catch (Exception ex)
      {
        output.Status = SystemStatus.ERROR;
        output.Message = "GM Service [GM/InsUserFeedBack] : " + ex.Message;
        return BadRequest(new { Message = output.Message + "" });
      }
      finally
      {
        Utility.Ins.WriteToLog("GM", "InsUserFeedBack", feedBackData, output);
      }

    }

    /*  [HttpPost]
      [Route("Gm/GetTRANBWC")]*/

        [HttpPost]
        [Route("Gm/GetTRANBWCFilter")]
        public IActionResult GetTRANBWCFilter([FromBody] TranBwcModel tranData)
        {
            var output = new OutputModel();
            try
            {
                output = TranBwcManagement.Ins.FilterTranBwc(tranData);

                if (output.Status == SystemStatus.SUCCESS)
                {
                    return Ok(output.Data);
                }
                else
                {
                    return BadRequest(new { Message = output.Message + "" });
                }
            }
            catch (Exception ex)
            {
                output.Status = SystemStatus.ERROR;
                output.Message = "GM Service [GM/CheckUser] : " + ex.Message;
                return BadRequest(new { Message = output.Message + "" });
            }
            finally
            {
                LogsModel logData = new LogsModel();
                logData.USER_NAME = "test123";
                logData.SERVICE_NAME = "GM/GetTRANBWCFilter";
                LogsManagement.Ins.LogsMng(logData);
                Utility.Ins.WriteToLog("GM", "GetTRANBWCFilter", "", output);
            }
        }

       
        [HttpGet]
        [Route("Gm/GetTRANBWC")]
        public IActionResult GetTRANBWC()
        {
            var output = new OutputModel();
            try
            {
                output = TranBwcManagement.Ins.TranBwcMng();

                if (output.Status == SystemStatus.SUCCESS)
                {
                    return Ok(output.Data);
                }
                else
                {
                    return BadRequest(new { Message = output.Message + "" });
                }
            }
            catch (Exception ex)
            {
                output.Status = SystemStatus.ERROR;
                output.Message = "GM Service [GM/CheckUser] : " + ex.Message;
                return BadRequest(new { Message = output.Message + "" });
            }
            finally
            {
                LogsModel logData = new LogsModel();
                logData.USER_NAME = "test123";
                logData.SERVICE_NAME = "GM/GetTRANBWC";
                LogsManagement.Ins.LogsMng(logData);
                Utility.Ins.WriteToLog("GM", "GetTRANBWC", "" , output);
            }
        }

    [HttpGet]
    [Route("Gm/GetTRANBWCProduct")]
    public IActionResult GetTRANBWCProduct()
    {
      var output = new OutputModel();
      try
      {
        output = TranBwcManagement.Ins.TranBwcProductMng();

        if (output.Status == SystemStatus.SUCCESS)
        {
          return Ok(output.Data);
        }
        else
        {
          return BadRequest(new { Message = output.Message + "" });
        }
      }
      catch (Exception ex)
      {
        output.Status = SystemStatus.ERROR;
        output.Message = "GM Service [GM/GetTRANBWCProduct] : " + ex.Message;
        return BadRequest(new { Message = output.Message + "" });
      }
      finally
      {
        LogsModel logData = new LogsModel();
        logData.USER_NAME = "test123";
        logData.SERVICE_NAME = "GM/GetTRANBWCProduct";
        LogsManagement.Ins.LogsMng(logData);
        Utility.Ins.WriteToLog("GM", "GetTRANBWCProduct", "", output);
      }
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("Gm/GetLogs")]
    public IActionResult GetLogs()
    {
      var output = new OutputModel();
      try
      {
        output = LogsManagement.Ins.LogSelMng();
        if (output.Status == SystemStatus.SUCCESS)
        {
          return Ok(output.Data);
        }
        else
        {
          return BadRequest(new { Message = output.Message + "" });
        }
      }
      catch (Exception ex)
      {
        output.Status = SystemStatus.ERROR;
        output.Message = "GM Service [Gm/Login] : " + ex.Message;
        return BadRequest(new { Message = output.Message + "" });
      }
      finally
      {
        Utility.Ins.WriteToLog("GM", "GetLogs", "", output);
      }

    }

  }
}
