#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["gmprojectbackend/gmprojectbackend.csproj", "gmprojectbackend/"]
COPY ["BussinessLogic/BussinessLogic.csproj", "BussinessLogic/"]
COPY ["DBConnection/DBConnection.csproj", "DBConnection/"]
COPY ["Management/Management.csproj", "Management/"]
COPY ["Model/Model.csproj", "Model/"]
RUN dotnet restore "gmprojectbackend/gmprojectbackend.csproj"
COPY . .
WORKDIR "/src/gmprojectbackend"
RUN dotnet build "gmprojectbackend.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "gmprojectbackend.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "gmprojectbackend.dll"]