﻿using BussinessLogic;
using gmprojectbackend.DBConnection;
using Model;
using Model.AppConfig;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Management
{
    public class TranBwcManagement
    {
        OutputModel output;

        SqlConnection sqlConn;

        SqlTransaction trans;

        private static TranBwcManagement _instance = null;

        public static TranBwcManagement Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TranBwcManagement();
                }

                return _instance;
            }
        }

        public OutputModel FilterTranBwc(TranBwcModel tranData)
        {

            try
            {
                string message = "";
                int rescode = SystemStatus.SUCCESS;
                TranBwcResponseModel data = null;

                sqlConn = DBConnection.Ins.DBConnect;
                sqlConn.Open();

                if (tranData != null) {
                    rescode = TranBwcLogic.Ins.TranbwcFilterLogic(sqlConn, tranData ,out data);
                }

                if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
                output = new OutputModel(rescode, message, data);
                return output;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                throw new Exception("FilterTranBwc " + ex.Message);
            }
            finally
            {
                DBConnection.Ins.SqlCloseConnection(sqlConn);
                sqlConn = null;
                trans = null;
            }

        }

        public OutputModel TranBwcMng()
        {
            try
            {
                string message = "";
                int rescode = SystemStatus.SUCCESS;
                TranBwcResponseModel data = null;

                sqlConn = DBConnection.Ins.DBConnect;
                sqlConn.Open();

                rescode = TranBwcLogic.Ins.TranbwcLogic(sqlConn, out data);
                if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
                output = new OutputModel(rescode, message, data);
                return output;
            }
            catch(Exception ex)
            {
                if (trans != null) trans.Rollback();
                throw new Exception("TranBwcMng " + ex.Message);
            }
            finally
            {
                DBConnection.Ins.SqlCloseConnection(sqlConn);
                sqlConn = null;
                trans = null;
            }
        }

    public OutputModel TranBwcProductMng()
    {
      try
      {
        string message = "";
        int rescode = SystemStatus.SUCCESS;
        TranBwcProductResponseModel data = null;

        sqlConn = DBConnection.Ins.DBConnect;
        sqlConn.Open();

        rescode = TranBwcLogic.Ins.TranbwcProductLogic(sqlConn, out data);
        if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
        output = new OutputModel(rescode, message, data);
        return output;
      }
      catch (Exception ex)
      {
        if (trans != null) trans.Rollback();
        throw new Exception("TranBwcProductMng " + ex.Message);
      }
      finally
      {
        DBConnection.Ins.SqlCloseConnection(sqlConn);
        sqlConn = null;
        trans = null;
      }
    }





  }
}
