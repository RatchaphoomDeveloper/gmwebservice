﻿using BussinessLogic;
using gmprojectbackend.DBConnection;
using Model;
using Model.AppConfig;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;

namespace Management
{
    public class RegisterManagement
    {
        OutputModel output;

        SqlConnection sqlConn;

        SqlTransaction trans;

        private static RegisterManagement _instance = null;

        public static RegisterManagement Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RegisterManagement();
                }

                return _instance;
            }
        }

        public OutputModel RegisterMng(UserModel userData)
        {
          try
          {
            string message = "";
            int rescode = SystemStatus.SUCCESS;
            string createDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));

            sqlConn = DBConnection.Ins.DBConnect;
            sqlConn.Open();
            trans = sqlConn.BeginTransaction();

            if (string.IsNullOrEmpty(userData.USER_NAME) || string.IsNullOrEmpty(userData.USER_COMAPANY) || string.IsNullOrEmpty(userData.USER_EMAIL) || string.IsNullOrEmpty(userData.USER_LASTNAME))
            {
              rescode = SystemStatus.INVALID_REGISTER_DATA;
            }

            if (rescode == SystemStatus.SUCCESS)
            {
              rescode = RegisterLogic.Ins.RegisterLogics(sqlConn, userData,trans);

            }

            if (rescode == SystemStatus.SUCCESS)
            {
              if (trans != null) trans.Commit();
            }
            else
            {
              if (trans != null) trans.Rollback();
            }
            if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
            output = new OutputModel(rescode, message, null);

            return output;
          }
          catch (Exception ex)
          {
            if (trans != null) trans.Rollback();
            throw new Exception("RegisterMng " + ex.Message);
          }
          finally {
            DBConnection.Ins.SqlCloseConnection(sqlConn);
            sqlConn = null;
            trans = null;

          }
        }
    }
}
