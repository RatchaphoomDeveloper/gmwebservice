﻿using BussinessLogic;
using gmprojectbackend.DBConnection;
using Model;
using Model.AppConfig;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Management
{
    public class LogsManagement
    {
        OutputModel output;

        SqlConnection sqlConn;

        SqlTransaction trans;

        private static LogsManagement _instance = null;

        public static LogsManagement Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogsManagement();
                }

                return _instance;
            }
        }

        public OutputModel LogSelMng()
        {
            try
            {
                string message = "";
                int rescode = SystemStatus.SUCCESS;
                LogsResponeModel data = null;

                sqlConn = DBConnection.Ins.DBConnect;
                sqlConn.Open();

                rescode = LogsLogic.Ins.LogsSelLogic(sqlConn, out data);

                if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
                output = new OutputModel(rescode, message, data);

                return output;
            }
            catch(Exception ex)
            {
                if (trans != null) trans.Rollback();
                throw new Exception("LogSelMng : " + ex.Message);
            }
            finally
            {
                DBConnection.Ins.SqlCloseConnection(sqlConn);
                sqlConn = null;
                trans = null;
            }
        }

        public OutputModel LogsMng(LogsModel logsData)
        {
            try
            {

                string message = "";
                int rescode = SystemStatus.SUCCESS;
                LogsResponeModel data = null;

                sqlConn = DBConnection.Ins.DBConnect;
                sqlConn.Open();
                trans = sqlConn.BeginTransaction();

                if (logsData != null)
                {
                    rescode = LogsLogic.Ins.LogsSaveLogic(sqlConn, logsData, trans);
                }

                if (rescode == SystemStatus.SUCCESS)
                {
                    if (trans != null) trans.Commit();
                }
                else
                {
                    if (trans != null) trans.Rollback();
                }


                if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
                output = new OutputModel(rescode, message, data);

                return output;
            }
            catch(Exception ex)
            {
                if (trans != null) trans.Rollback();
                throw new Exception("LogsMng : " + ex.Message);
            }
            finally
            {
                DBConnection.Ins.SqlCloseConnection(sqlConn);
                sqlConn = null;
                trans = null;
            }
        }

    }
}
