﻿using BussinessLogic;
using gmprojectbackend.DBConnection;
using Model;
using Model.AppConfig;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Management
{
    public class LoginManagement
    {
        OutputModel output;
        SqlConnection sqlConn;
        SqlTransaction trans;

        private static LoginManagement _instance = null;

        public static LoginManagement Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LoginManagement();
                }

                return _instance;
            }
        }

    public OutputModel CheckUserStatusMng(UserStatusModel userStatusData) {
      try
      {
        string message = "";
        int rescode = SystemStatus.SUCCESS;
        UserStatusResponseModel data = null;
        sqlConn = DBConnection.Ins.DBConnect;
        sqlConn.Open();
       
        if (userStatusData != null) {
          rescode = LoginLogic.Ins.CheckUserStatusLogic(sqlConn, userStatusData, out data);
        }

        if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
        output = new OutputModel(rescode, message, data);
        return output;

      }
      catch(Exception ex)
      {
        if (trans != null) trans.Rollback();
        throw new Exception("CheckUserStatusMng " + ex.Message);
      }
      finally
      {
        DBConnection.Ins.SqlCloseConnection(sqlConn);
        sqlConn = null;
        trans = null;
      }
    }

    public OutputModel InsUserFeedBack(FeedBackModel feebackData)
    {
      try
      {
        string message = "";
        int rescode = SystemStatus.SUCCESS;
        FeedBackResponseModel data = null;

        sqlConn = DBConnection.Ins.DBConnect;
        sqlConn.Open();
        trans = sqlConn.BeginTransaction();

        if (feebackData != null) {
          rescode = LoginLogic.Ins.InsFeedBackLogic(sqlConn, feebackData, trans);
        }

        if (rescode == SystemStatus.SUCCESS)
        {
          if (trans != null) trans.Commit();
        }
        else
        {
          if (trans != null) trans.Rollback();
        }

        if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
        output = new OutputModel(rescode, message, data);
        return output;

      }
      catch (Exception ex)
      {
        if (trans != null) trans.Rollback();
        throw new Exception("InsUserFeedBack " + ex.Message);
      }
      finally
      {
        DBConnection.Ins.SqlCloseConnection(sqlConn);
        sqlConn = null;
        trans = null;
      }
    }

        public OutputModel CheckUserMng(LoginModel loginData) {

            try {
                string message = "";
                int rescode = SystemStatus.SUCCESS;
                LoginResponseModel data = null;

                sqlConn = DBConnection.Ins.DBConnect;
                sqlConn.Open();

                if (loginData != null)
                {
                    rescode = LoginLogic.Ins.CheckUserLogic(sqlConn, loginData, out data);
                }


                if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
                output = new OutputModel(rescode, message, data);
                return output;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                throw new Exception("CheckUserMng " + ex.Message);
            }
            finally
            {
                DBConnection.Ins.SqlCloseConnection(sqlConn);
                sqlConn = null;
                trans = null;

            }
        }

        public OutputModel ChangePasswordMng(LoginModel loginData)
        {

            try
            {
                string message = "";
                int rescode = SystemStatus.SUCCESS;
                LoginResponseModel data = null;

                sqlConn = DBConnection.Ins.DBConnect;
                sqlConn.Open();
                trans = sqlConn.BeginTransaction();

                if (loginData != null)
                {
                    rescode = LoginLogic.Ins.ChangePasswordLogic(sqlConn, loginData,trans);
                }

                if (rescode == SystemStatus.SUCCESS)
                {
                    if (trans != null) trans.Commit();
                }
                else
                {
                    if (trans != null) trans.Rollback();
                }

                if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
                output = new OutputModel(rescode, message, data);

                return output;
            }
            catch (Exception ex)
            {
                if (trans != null) trans.Rollback();
                throw new Exception("ChangePasswordMng " + ex.Message);
            }
            finally
            {
                DBConnection.Ins.SqlCloseConnection(sqlConn);
                sqlConn = null;
                trans = null;

            }
        }

        public OutputModel LoginMng(LoginModel loginData)
        {
            try
            {
                string message = "";
                int rescode = SystemStatus.SUCCESS;
                LoginResponseModel data = null;

                sqlConn = DBConnection.Ins.DBConnect;
                sqlConn.Open();

                if (loginData != null) {
                    rescode = LoginLogic.Ins.LoginLogics(sqlConn,loginData,out data);
                }
                else
                {

                }


                if(rescode == SystemStatus.SUCCESS)
                {
                    data.UserData[0].Token = AppSettings.Ins.GenerateToken(data.UserData[0].USER_NAME);
                }

                if (string.IsNullOrEmpty(message)) message = Messengers.Ins.GetMessage(rescode);
                output = new OutputModel(rescode, message, data);

                return output;

            }catch(Exception ex)
            {
                throw new Exception("LoginMobileMng : " + ex.Message);
            }
            finally
            {
                DBConnection.Ins.SqlCloseConnection(sqlConn);
                sqlConn = null;
            }

        }
    }
}
