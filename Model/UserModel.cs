﻿using System;

namespace Model
{
    public class UserModel
    {
        public int ID { get; set; }
        public string UUID { get; set; }
        public string USER_ID { get; set; }
        public string USER_PASSWORD { get; set; }
        public string USER_EMAIL { get; set; }
        public string USER_NAME { get; set; }
        public string USER_LASTNAME { get; set; }
        public string  USER_COMAPANY { get; set; }
    }
}
