﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class LoginModel{
        public string UserId { get; set; }
        public string UserPassword { get; set; }
    }

    public class LoginResponseModel
    {
        public List<LoginUserData> UserData { get; set; }
        
    }

    public class LoginUserData
    {
        
        [JsonProperty("UUID")]
        public string UUID { get; set; }
        [JsonProperty("TOKEN")]
        public string Token { get; set; }
        [JsonProperty("USER_EMAIL")]
        public string USER_EMAIL { get; set; }
        [JsonProperty("USER_NAME")]
        public string USER_NAME { get; set; }
        [JsonProperty("USER_LASTNAME")]
        public string USER_LASTNAME { get; set; }
        [JsonProperty("USER_COMPANY")]
        public string USER_COMAPANY { get; set; }
    }
}
