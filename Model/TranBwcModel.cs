﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class TranBwcModel
    {
        public string companyname { get; set; }
        public string companycode { get; set; }
    }

    public class TranBwcProductModel {


    }

    public class TranBwcProductResponseModel {
      public List<TranBwcProductData> TranBwcProductData { get; set; }
    }

    public class TranBwcResponseModel
    {
        public List<TranBwcData> TranBwcData { get; set; }
    }


    public class TranBwcData
    {

        [JsonProperty("UUID")]
        public string UUID { get; set; }
        [JsonProperty("NAME")]
        public string NAME { get; set; }
        [JsonProperty("CODE")]
        public string CODE { get; set; }
        [JsonProperty("STATUS_ID")]
        public string STATUS_ID { get; set; }
        [JsonProperty("STATUS_NAME")]
        public string STATUS_NAME { get; set; }

    }


    public class TranBwcProductData
    {
      [JsonProperty("PRODUCTCODE")]
      public string PRODUCTCODE { get; set; }
      [JsonProperty("XTYPE")]
      public string XTYPE { get; set; }
  }
}
