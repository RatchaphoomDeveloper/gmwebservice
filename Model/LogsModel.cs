﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class LogsModel
    {
        public string UUID { get; set; }
        public string USER_NAME { get; set; }
        public string SERVICE_NAME { get; set; }
    }

    public class LogsResponeModel
    {
        public List<LogsData> LogsData { get; set; }
    }

    public class LogsData
    {
        [JsonProperty("UUID")]
        public string UUID { get; set; }

        [JsonProperty("USER_NAME")]
        public string USER_NAME { get; set; }

        [JsonProperty("SERVICE_NAME")]
        public string SERVICE_NAME { get; set; }
    }
}
