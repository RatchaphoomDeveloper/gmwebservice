using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Model
{
  public class FeedBackModel
  {
    public string UUID { get; set; }
    public string FEEDBACK { get; set; }
  }

  public class FeedBackResponseModel
  {
    public List<FeedBackData> FeedBackData { get; set; }

  }

  public class FeedBackData
  {

    [JsonProperty("UUID")]
    public string UUID { get; set; }
    [JsonProperty("TOKEN")]
    public string Token { get; set; }
    [JsonProperty("USER_EMAIL")]
    public string USER_EMAIL { get; set; }
    [JsonProperty("USER_NAME")]
    public string USER_NAME { get; set; }
    [JsonProperty("USER_LASTNAME")]
    public string USER_LASTNAME { get; set; }
    [JsonProperty("USER_COMPANY")]
    public string USER_COMAPANY { get; set; }
  }
}
