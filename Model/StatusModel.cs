using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class UserStatusModel
    {
        public string UUID { get; set; }
        public string USER_NAME { get; set; }
    }

    public class UserStatusResponseModel
    {
        public List<UserStatusData> UserStatusData { get; set; }
    }

    public class UserStatusData
    {
        [JsonProperty("UUID")]
        public string UUID { get; set; }

        [JsonProperty("USER_STATUS")]
        public string USER_STATUS { get; set; }

        [JsonProperty("APPROVE_STATUS")]
        public string APPROVE_STATUS { get; set; }

        [JsonProperty("THAI_APPROVE_STATUS")]
        public string THAI_APPROVE_STATUS { get; set; }
  }
}
