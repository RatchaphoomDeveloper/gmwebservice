﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.AppConfig
{
    public class Messengers
    {

        private static Messengers _instance = null;
        
        public static Messengers Ins
        {
            get
            {
                if (_instance == null) {

                    _instance = new Messengers();
                }
                return _instance;
            }
        }

        public string GetMessage(int rescode,string value = "")
        {
            try
            {
                if (rescode == SystemStatus.SUCCESS) return "ทำรายการสำเร็จ";
                if (rescode == SystemStatus.PROCESS_FAILED) return "ไม่สำเร็จ กรุณาทำรายการใหม่หรือติดต่อผู้ดูแลระบบ";
                if (rescode == SystemStatus.DATA_NOT_FOUND) return "ไม่พบข้อมูล";
                if (rescode == SystemStatus.INVALID_USER) return "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง";
                if (rescode == SystemStatus.INVALID_REGISTER_DATA) return "กรุณาระบุข้อมูลให้ครบทุกช่อง";
                if (rescode == SystemStatus.USER_IS_IN_SYSTEM) return "ชื่อผู้ใช้งานซ้ำกรุณาเปลี่ยนชื่อผู้ใช้งาน หรือติดต่อผู้ดูแลระบบ";
                if (rescode == SystemStatus.INSERT_FEEDBACK_ERROR) return "ชื่อผู้ใช้งานของท่านไม่ตรงกับต้นทาง หรือติดต่อผู้ดูแลระบบ";
                if (rescode == SystemStatus.USER_PADDING) return "ชื่อผู้ใช้งานของท่านยังไม่ได้รับการอนุมัติจากผู้ดูแลระบบ";


        return "ไม่พบคำอธิบายสำหรับข้อผิดพลาดนี้ กรุณาทำรายการใหม่หรือติดต่อผู้ดูแลระบบ";
            }
            catch (Exception ex) {
                throw new Exception("" + ex.Message);
            }
        }

    }
}
