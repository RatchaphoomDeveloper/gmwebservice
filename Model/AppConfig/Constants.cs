﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.AppConfig
{
   public class SystemStatus
    {
        public const int SUCCESS = 1;
        public const int ERROR = -5;
        public const int DATA_NOT_FOUND = -10;
        public const int INVALID_USER = -20;
        public const int PROCESS_FAILED = -30;
        public const int INVALID_REGISTER_DATA = -23;
        public const int USER_IS_IN_SYSTEM = -24;
        public const int INSERT_FEEDBACK_ERROR = -25;
        public const int USER_PADDING = -26;
        
    }
}
