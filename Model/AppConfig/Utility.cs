﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Model.AppConfig
{
    public class Utility
    {

        private static Utility _instance = null;

        public static Utility Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Utility();
                }

                return _instance;
            }
        }

        public JsonSerializerSettings GetJsonSettings()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                //MissingMemberHandling = MissingMemberHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss",
                Formatting = Newtonsoft.Json.Formatting.Indented
            };

            return settings;
        }
        public void WriteToLog(string systemType,string serviceName,object input,OutputModel output)
        {
            try
            {
                string logPath = Directory.GetCurrentDirectory() + "/_log/";
                if (!Directory.Exists(logPath)) Directory.CreateDirectory(logPath);
                string status;

                if(output.Status == SystemStatus.SUCCESS)
                {
                    status = "Success";
                    if (serviceName == "Login") {
                        string filename = DateTime.Now.ToString("yyyyMMdd_HHmmssfff", new CultureInfo("en-US")) + "_" + status + "_" + serviceName + ".txt";
                        string inputData = JsonConvert.SerializeObject(input);
                        string outputData = JsonConvert.SerializeObject(output.Data);

                        var log = new StreamWriter(logPath + filename);
                        log.WriteLine("Action Service : GM Service [" + systemType + "/" + serviceName + "]");
                        log.WriteLine("----------------------------------------------------------------------------------------");
                        log.WriteLine("Status : " + status + " (" + output.Status + ")");
                        log.WriteLine("----------------------------------------------------------------------------------------");
                        log.WriteLine("Message : " + output.Message);
                        log.WriteLine("----------------------------------------------------------------------------------------");
                        log.WriteLine("Input Data : " + inputData);
                        log.WriteLine("----------------------------------------------------------------------------------------");
                        log.WriteLine("Output Data : " + outputData);
                        log.Close();
                    }
                    else
                    {
                        //Log error
                        status = "Failed";

                        //Set log data
                        string filename = DateTime.Now.ToString("yyyyMMdd_HHmmssfff", new CultureInfo("en-US")) + "_" + status + "_" + serviceName + ".txt";
                        string inputData = JsonConvert.SerializeObject(input);

                        //Write to file
                        var log = new StreamWriter(logPath + filename);
                        log.WriteLine("Action Service : GM Service [" + systemType + "/" + serviceName + "]");
                        log.WriteLine("----------------------------------------------------------------------------------------");
                        log.WriteLine("Status : " + status + " (" + output.Status + ")");
                        log.WriteLine("----------------------------------------------------------------------------------------");
                        log.WriteLine("Message : " + output.Message);
                        log.WriteLine("----------------------------------------------------------------------------------------");
                        log.WriteLine("Input Data : " + inputData);
                        log.Close();
                    }
                }

            }
            catch(Exception ex)
            {
                string msg = ex.Message;
            }
        }
    }
}
