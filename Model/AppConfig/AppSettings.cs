﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;

namespace Model.AppConfig
{
    public class AppSettings
    {
        private static AppSettings _instance = null;

        public static AppSettings Ins
        {
            get
            {
                if (_instance == null ) {
                    _instance = new AppSettings();
                }
                return _instance;
            }
        }

        private IConfigurationRoot GetConfigData() {
            try
            {
                var builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                return builder.Build();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetSqlConnStr()
        {
            try
            {
                var configuration = GetConfigData();
                string connStr = configuration.GetSection("SqlServerConnectionStr").Value;
                return connStr;
            }
            catch(Exception ex)
            {
                return "";
            }
        }

        public string GetSqlSchema()
        {
            try
            {
                var configuration = GetConfigData();
                string schrema = configuration.GetSection("SqlSchema").Value;

                return schrema;
            }
            catch(Exception)
            {
                return "";
            }
        }

        public string GenerateToken(string user)
        {
            try
            {
                var configuration = GetConfigData();
                string key = configuration.GetSection("Authentication").GetSection("Key").Value;
                string issuer = configuration.GetSection("Authentication").GetSection("Issuer").Value;

                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub,user),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
                };

                //Create token
                var jwtToken = new JwtSecurityToken(
                    issuer: issuer,
                    audience: issuer,
                    claims,
                    expires: DateTime.Now.AddMonths(4), //Set expire token
                    signingCredentials: credentials);

                string token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

                return token;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
