﻿using System;
using System.Data.SqlClient;
using Model.AppConfig;
namespace gmprojectbackend.DBConnection
{
    public class DBConnection
    {
        private static DBConnection _instance = null;

        public static DBConnection Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DBConnection();
                }

                return _instance;
            }

        }


        public SqlConnection DBConnect
        {
            get
            {
                try
                {
                    SqlConnection sqlConn = null;
                    string connstr = AppSettings.Ins.GetSqlConnStr();
                    sqlConn = new SqlConnection(connstr);
                    return sqlConn;
                }
                catch (Exception ex)
                {
                    throw new Exception("MSQLConnection/DBConnection " + ex.Message);
                }
            }
        }

        public void SqlCloseConnection(SqlConnection sql)
        {
            try
            {
                if (sql != null)
                {
                    if(sql.State == System.Data.ConnectionState.Open)
                    {
                        sql.Close();
                    }
                }
            }catch(Exception ex)
            {
                
            }
        }
    }
}
