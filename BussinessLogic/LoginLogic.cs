﻿using Model;
using Model.AppConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BussinessLogic
{
    public class LoginLogic
    {
        string schema = AppSettings.Ins.GetSqlSchema();
        private static LoginLogic _instance = null;

        public static LoginLogic Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LoginLogic();
                }

                return _instance;
            }
        }

        public int CheckUserStatusLogic(SqlConnection sqlConn,UserStatusModel userStatusData,out UserStatusResponseModel data)
        {
          try{
              DataSet ds;
              int rescode;
              data = null;
              SqlCommand cmd = new SqlCommand();
              cmd.Connection = sqlConn;
              cmd.CommandText = "SP_BWC_CHECK_USER_STATUS";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.CommandTimeout = 60;

              
              cmd.Parameters.Add("@uuid", SqlDbType.NVarChar).Value = userStatusData.UUID;
              cmd.Parameters.Add("@RETURN_CODE",SqlDbType.Int).Direction = ParameterDirection.Output;
              ds = new DataSet();
              using (SqlDataAdapter adapter = new SqlDataAdapter())
              {
                adapter.SelectCommand = cmd;
                adapter.Fill(ds);
              }
              if (ds.Tables[0].Rows.Count > 0)
              {

                rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                if (rescode == SystemStatus.SUCCESS)
                {
                  ds.Tables[0].TableName = "userStatusData";

                  var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
                  data = JsonConvert.DeserializeObject<UserStatusResponseModel>(jsonData);
                }
              }
              else
              {
                rescode = SystemStatus.DATA_NOT_FOUND;
              }

              return rescode;

          }
          catch(Exception ex)
          {
                throw new Exception("CheckUserStatus : " + ex.Message);
          }
        }


        public int InsFeedBackLogic(SqlConnection sqlConn, FeedBackModel feedBackData, SqlTransaction trans) {

            try
            {
              int rescode;
              SqlCommand cmd = new SqlCommand();
              cmd.Connection = sqlConn;
              cmd.CommandText = "BWC_USER_FEEDBACK_INSERT";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.CommandTimeout = 60;
              cmd.Transaction = trans;

              cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
              cmd.Parameters.Add("@uuid", SqlDbType.NVarChar).Value = feedBackData.UUID;
              cmd.Parameters.Add("@feedback", SqlDbType.NVarChar).Value = feedBackData.FEEDBACK;
              cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;
              cmd.ExecuteNonQuery();

              rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
              return rescode;
            }
            catch (Exception ex)
            {
              throw new Exception("InsFeedBackLogic : " + ex.Message);
            }
        }


        

        public int CheckUserLogic(SqlConnection sqlConn, LoginModel loginData, out LoginResponseModel data) {
            try
            {
                DataSet ds;
                int rescode;
                data = null;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "GM_CHANGE_PASSWORD_GET_USER_SEL";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;

                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
                cmd.Parameters.Add("@user_email", SqlDbType.NVarChar).Value = loginData.UserId;
                cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;

                ds = new DataSet();
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);
                }

                if (ds.Tables[0].Rows.Count > 0)
                {

                    rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                    if (rescode == SystemStatus.SUCCESS)
                    {
                        ds.Tables[0].TableName = "UserData";

                        var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
                        data = JsonConvert.DeserializeObject<LoginResponseModel>(jsonData);
                    }
                }
                else
                {
                    rescode = SystemStatus.INVALID_USER;
                }


                return rescode;
            }
            catch (Exception ex)
            {
                throw new Exception("LoginLogic : " + ex.Message);
            }

        }

        public int ChangePasswordLogic(SqlConnection sqlConn, LoginModel loginData, SqlTransaction trans) {

            try
            {
                int rescode;
                Guid myuuid = Guid.NewGuid();
                string myuuidAsString = myuuid.ToString();


                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "GM_CHANGE_PASSWORD_INSERT";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                cmd.Transaction = trans;

                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
                cmd.Parameters.Add("@uuid", SqlDbType.NVarChar).Value = loginData.UserId;
                cmd.Parameters.Add("@user_password", SqlDbType.NVarChar).Value = loginData.UserPassword;
                cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                return rescode;
            }
            catch (Exception ex)
            {
                throw new Exception("ChangePasswordLogic : " + ex.Message);
            }


        }

        public int LoginLogics(SqlConnection sqlConn,LoginModel loginData,out LoginResponseModel data) {
            try
            {
                DataSet ds;
                int rescode;
                data = null;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "GM_LOGIN";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;

/*                cmd.Parameters.Add("@O_USER_CURSOR", SqlDbType.Real, 500);
*/              cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
                cmd.Parameters.Add("@user_id", SqlDbType.NVarChar).Value = loginData.UserId;
                cmd.Parameters.Add("@user_password", SqlDbType.NVarChar).Value = loginData.UserPassword;
                cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;
                /*cmd.Parameters.Add("@O_USER_CURSOR", SqlDbType.Variant).Direction = ParameterDirection.Output;*/
/*                cmd.ExecuteNonQuery();
*/
                ds = new DataSet();
                using(SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);
                }

                rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                if (ds.Tables[0].Rows.Count > 0) {
                    

                    if (rescode == SystemStatus.SUCCESS)
                    {
                        ds.Tables[0].TableName = "UserData";

                        var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
                        data = JsonConvert.DeserializeObject<LoginResponseModel>(jsonData);
                    }
                    if (rescode == SystemStatus.USER_PADDING)
                    {
                      rescode = SystemStatus.USER_PADDING;
                    }
                }
             
                else
                {
                    rescode = SystemStatus.INVALID_USER;
                }

                
                return rescode;
            }
            catch (Exception ex)
            {
                throw new Exception("LoginLogic : " + ex.Message);
            }
        }
    }
}
