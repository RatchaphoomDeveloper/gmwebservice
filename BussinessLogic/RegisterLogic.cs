﻿using Model;
using Model.AppConfig;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BussinessLogic
{
    public class RegisterLogic
    {
        private static RegisterLogic _instance = null;

        public static RegisterLogic Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RegisterLogic();
                }

                return _instance;
            }
        }

        public int RegisterLogics(SqlConnection sqlConn,UserModel userData,SqlTransaction trans ) {
            try
            {
                int rescode;
                Guid myuuid = Guid.NewGuid();
                string myuuidAsString = myuuid.ToString();


                SqlCommand cmd = new SqlCommand();        
                cmd.Connection = sqlConn;
                cmd.CommandText = "GM_REGISTER";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                cmd.Transaction = trans;
                
  


                cmd.Parameters.Add("@uuid", SqlDbType.NVarChar).Value = myuuidAsString.ToString();
                cmd.Parameters.Add("@user_name", SqlDbType.NVarChar).Value = userData.USER_NAME;
                cmd.Parameters.Add("@user_password", SqlDbType.NVarChar).Value = userData.USER_PASSWORD;
                cmd.Parameters.Add("@user_email", SqlDbType.NVarChar).Value = userData.USER_EMAIL;
                cmd.Parameters.Add("@user_id", SqlDbType.NVarChar).Value = userData.USER_ID;
                cmd.Parameters.Add("@user_lastname", SqlDbType.NVarChar).Value = userData.USER_LASTNAME;
                cmd.Parameters.Add("@user_company", SqlDbType.NVarChar).Value = userData.USER_COMAPANY;
                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                if (rescode == -24)
                {
                  rescode = SystemStatus.USER_IS_IN_SYSTEM;
                }

                return rescode;
            }catch(Exception ex)
            {
                throw new Exception(" Save Register " + ex.Message);
            }
        }
    }
}
