﻿using Model;
using Model.AppConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BussinessLogic
{
    public class LogsLogic
    {
        string schema = AppSettings.Ins.GetSqlSchema();
        private static LogsLogic _instance = null;

        public static LogsLogic Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogsLogic();
                }

                return _instance;
            }
        }

        public int LogsSaveLogic(SqlConnection sqlConn, LogsModel logsData, SqlTransaction trans) {
            try
            {

                int rescode;
                Guid myuuid = Guid.NewGuid();
                string myuuidAsString = myuuid.ToString();


                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "GM_VW_API_CALL_LOG_INSERT";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                cmd.Transaction = trans;

                cmd.Parameters.Add("@uuid", SqlDbType.NVarChar).Value = myuuidAsString.ToString();
                cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = logsData.USER_NAME;
                cmd.Parameters.Add("@service_name", SqlDbType.NVarChar).Value = logsData.SERVICE_NAME ;
                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();

                rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());

                return rescode;
            }
            catch(Exception ex)
            {
                throw new Exception(" LogsSaveLogic " + ex.Message);
            }
        }

        public int LogsSelLogic(SqlConnection sqlConn, out LogsResponeModel data)
        {
            try
            {
                DataSet ds;
                int rescode;
                data = null;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "GM_VW_API_CALL_LOG_SEL";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;

                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
                cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;

                ds = new DataSet();
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);
                }

                if (ds.Tables[0].Rows.Count > 0)
                {

                    rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                    if (rescode == SystemStatus.SUCCESS)
                    {
                        ds.Tables[0].TableName = "LogsData";

                        var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
                        data = JsonConvert.DeserializeObject<LogsResponeModel>(jsonData);
                    }
                }
                else
                {
                    rescode = SystemStatus.DATA_NOT_FOUND;
                }


                return rescode;
            }
            catch (Exception ex)
            {
                throw new Exception("LogsSelLogic : " + ex.Message);
            }
        }
    }
}
