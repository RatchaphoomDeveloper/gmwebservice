﻿using Model;
using Model.AppConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BussinessLogic
{
    public class TranBwcLogic
    {
        private static TranBwcLogic _instance = null;

        public static TranBwcLogic Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TranBwcLogic();
                }

                return _instance;
            }
        }


        public int TranbwcFilterLogic(SqlConnection sqlConn, TranBwcModel tranModel ,out TranBwcResponseModel data)
        {
            try
            {
                DataSet ds;
                int rescode;
                data = null;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "VW_TRAN_BWC_FILTER";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;


                cmd.Parameters.Add("@companyname", SqlDbType.NVarChar).Value = tranModel.companyname;
                cmd.Parameters.Add("@companycode", SqlDbType.NVarChar).Value = tranModel.companycode;
                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
                cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;

                ds = new DataSet();
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);
                }

                if (ds.Tables[0].Rows.Count > 0)
                {

                    rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                    if (rescode == SystemStatus.SUCCESS)
                    {
                        ds.Tables[0].TableName = "TranBwcData";

                        var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
                        data = JsonConvert.DeserializeObject<TranBwcResponseModel>(jsonData);
                    }
                }
                else
                {
                    rescode = SystemStatus.DATA_NOT_FOUND;
                }


                return rescode;
            }
            catch (Exception ex)
            {
                throw new Exception("LoginLogic : " + ex.Message);
            }

        }

        public int TranbwcLogic(SqlConnection sqlConn ,out TranBwcResponseModel data) {
            try
            {
                DataSet ds;
                int rescode;
                data = null;
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandText = "VW_TRAN_BWC";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;

                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
                cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;
           
                ds = new DataSet();
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);
                }

                if (ds.Tables[0].Rows.Count > 0)
                {

                    rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                    if (rescode == SystemStatus.SUCCESS)
                    {
                        ds.Tables[0].TableName = "TranBwcData";

                        var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
                        data = JsonConvert.DeserializeObject<TranBwcResponseModel>(jsonData);
                    }
                }
                else
                {
                    rescode = SystemStatus.DATA_NOT_FOUND;
                }


                return rescode;
            }
            catch (Exception ex)
            {
                throw new Exception("LoginLogic : " + ex.Message);
            }

        }


    public int TranbwcProductLogic(SqlConnection sqlConn, out TranBwcProductResponseModel data)
    {
      try
      {
        DataSet ds;
        int rescode;
        data = null;
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = sqlConn;
        cmd.CommandText = "TRAN_BWC_IFRAME_PRODUCT_SEL";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = 60;

        cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Char, 500);
        cmd.Parameters["@RETURN_CODE"].Direction = ParameterDirection.Output;

        ds = new DataSet();
        using (SqlDataAdapter adapter = new SqlDataAdapter())
        {
          adapter.SelectCommand = cmd;
          adapter.Fill(ds);
        }

        if (ds.Tables[0].Rows.Count > 0)
        {

          rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
          if (rescode == SystemStatus.SUCCESS)
          {
            ds.Tables[0].TableName = "TranBwcProductData";

            var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
            data = JsonConvert.DeserializeObject<TranBwcProductResponseModel>(jsonData);
          }
        }
        else
        {
          rescode = SystemStatus.DATA_NOT_FOUND;
        }


        return rescode;
      }
      catch (Exception ex)
      {
        throw new Exception("TranbwcProductLogic : " + ex.Message);
      }

    }



  }

 

}

